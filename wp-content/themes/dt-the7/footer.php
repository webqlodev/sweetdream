<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the <div class="wf-container"> and all content after
 *
 * @package The7
 * @since 1.0.0
 */

// File Security Check
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( presscore_is_content_visible() ): ?>
    <div class="footer-section container-fluid p-0">
        <div class="footer-inner-section container">
            <div class="row">
                <div class="footer_logo col-4 text-center d-none d-lg-block">
                    <img src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/03/logo.png" alt="sweetdream_logo" class="lazy-load vc_single_image-img attachment-full is-loaded">
                </div>
                <div class="footer_links col-4 d-none d-lg-flex">
                    <div class="row m-auto">
                        <div class="col-6 pl-0">
                            <a href="<?php echo get_home_url(); ?>" rel="home page" class="bold">HOME</a>
                            <div class="about-us-title bold">ABOUT US</div>
                            <ul id="about-us" class="collape-footer-menu">
                                <li><a href="<?php echo get_home_url(); ?>/about-sweetdream/" rel="our story page" class="bold">About Sweetdream</a></li>
                                <li><a href="<?php echo get_home_url(); ?>/the-story/" rel="the story page" class="bold">The Story</a></li>
                                <li><a href="<?php echo get_home_url(); ?>/go-green/" rel="go green page" class="bold">Go Green</a></li>
                                <li><a href="<?php echo get_home_url(); ?>/reduce-co2/" rel="go reduce co2" class="bold">Reduce CO2</a></li>
                            </ul>
                            <a href="<?php echo get_home_url(); ?>/series/" rel="series page" class="bold">SERIES</a>
                            <div class="bold finesse-title">FINESSE</div>
                            <ul id="finesse" class="collape-footer-menu">
                                <li><a href="<?php echo get_home_url(); ?>/biorytmic-fabric/" rel="Biorytmic Fabric page">Biorytmic Fabric</a></li>
                                <li><a href="<?php echo get_home_url(); ?>/magnerest-fabric/" rel="Magnerest Fabric page">Magnerest Fabric</a></li>
                                <li><a href="<?php echo get_home_url(); ?>/cooler/" rel="Cooler page">Cooler Fabric</a></li>
                            </ul>
                            <a href="<?php echo get_home_url(); ?>/contact-us/" rel="contact us page" class="bold">CONTACT US</a>
                        </div>
                        <div class="col-6 pr-0 custom-padding-left">
                            <a href="<?php echo get_home_url(); ?>/warranty-registration/" rel="warranty page" class="bold">WARRANTY</a>
                            <div class="bold buy-online-title">BUY ONLINE</div>
                            <ul id="buy-online" class="collape-footer-menu">
                                <li><a href="https://www.lazada.com.my/shop/sweetdreamofficial/?langFlag=en&pageTypeId=1" rel="lazada" target="_blank" class="bold">Lazada</a></li>
                                <li><a href="https://shopee.com.my/sweetdreamofficial" target="_blank" rel="shopee" class="bold">Shopee</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="footer_details col-lg-4 col-12">
                    <div class="mobile-logo pb-3"><img src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/03/SDIC-logo-1.png" alt="SDIC_logo" class="lazy-load vc_single_image-img attachment-full is-loaded"></div>
                    <p>SweetDream Industrial Corporation Sdn Bhd</p>
                    <p>Lot 9, Jalan Perusahaan 1, Kawasan Perusahaan Beranang, 43700, Selangor, Malaysia</p>
                    <p><span class="bold">Email: </span><a href="mailto:info@sweetdream.com.my" rel="info email" class="email_link">info@sweetdream.com.my</a></p>
                    <p><span class="bold">Tel:</span> +1300 88 9921 // +603 8766 8129</p>
                    <a href="https://www.facebook.com/SweetDreamMsia/" rel="SweetDream facebook page" target="_blank"><span><img src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/03/facebook.png" alt="facebook" class="lazy-load vc_single_image-img attachment-full is-loaded"></span></a>
                    <a href="https://www.instagram.com/sweetdreammsia/" rel="SweetDream instagram page" target="_blank"><span><img src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/03/instagram.png" alt="instagram" class="lazy-load vc_single_image-img attachment-full is-loaded"></span></a>
                </div>
                <div class="footer_copyright col-12 d-lg-none d-block text-center">
                    <p>&#169;2019 SweetDream. All Rights Reserved.</p>
                </div>
            </div>
        </div>
    </div>
			</div><!-- .wf-container -->
		</div><!-- .wf-wrap -->
	<?php
	/**
	 * @since 6.8.1
	 */
	do_action( 'the7_after_content_container' );
	?>

	</div><!-- #main -->

	<?php
	if ( presscore_config()->get( 'template.footer.background.slideout_mode' ) ) {
		echo '</div>';
	}
	?>

<?php endif // presscore_is_content_visible ?>

	<?php do_action( 'presscore_after_main_container' ) ?>

	<a href="#" class="scroll-top"><span class="screen-reader-text"><?php esc_html_e( 'Go to Top', 'the7mk2' ) ?></span></a>

</div><!-- #page -->

<?php wp_footer() ?>

<div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="pswp__bg"></div>
    <div class="pswp__scroll-wrap">
 
        <div class="pswp__container">
            <div class="pswp__item"></div>
            <div class="pswp__item"></div>
            <div class="pswp__item"></div>
        </div>
 
        <div class="pswp__ui pswp__ui--hidden">
            <div class="pswp__top-bar">
                <div class="pswp__counter"></div>
                <button class="pswp__button pswp__button--close" title="Close (Esc)"></button>
                <button class="pswp__button pswp__button--share" title="Share"></button>
                <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>
                <button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>
                <div class="pswp__preloader">
                    <div class="pswp__preloader__icn">
                      <div class="pswp__preloader__cut">
                        <div class="pswp__preloader__donut"></div>
                      </div>
                    </div>
                </div>
            </div>
            <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
                <div class="pswp__share-tooltip"></div> 
            </div>
            <button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)">
            </button>
            <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)">
            </button>
            <div class="pswp__caption">
                <div class="pswp__caption__center"></div>
            </div>
        </div>
    </div>
</div>
</body>
</html>