<?php
/*
	Template Name: warranty-registration-template
*/
get_header();
?>
<div class="warranty-section container">
	<div class="warranty-inner-section">
		<div class="warranty-title-section text-center pb-4">
			<h2 class="text-uppercase">ACTIVATE YOUR WARRANTY NOW</h2>
			<p>Make sure you do this now to ensure that your purchase is 100% covered.</p>
		</div>
		<?php echo do_shortcode( '[contact-form-7 id="12" title="warranty_registration_form"]' ); ?>
	</div>
</div>
<?php get_footer() ?>