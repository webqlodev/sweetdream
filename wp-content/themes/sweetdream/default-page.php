<?php
/*
	Template Name: default-page-template
*/
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSS -->
    <style>
        body {
            background-color: #E6E6E6;
            margin: 0px;
        }
        .maintenance-div {
            text-align: center;
            padding: 0px 30px;
        }
        .maintenance-img {
            max-width: 700px;
            width: 100%;
        }
    </style>

    <title> SweetDream | Malaysia</title>
</head>

<body>

    <div class="maintenance-div">
        <img class="maintenance-img" src="<?php echo get_home_url(); ?>/wp-content/themes/sweetdream/img/sd_maintainancepage_1.png">
    </div>
    
</body>
</html>