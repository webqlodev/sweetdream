<?php
/*
 * Template Name: Article Template
 * Template Post Type: post
 */
get_header();
global $post;
?>
<div class="article-section container-fluid p-0">
	<div class="article-inner-section">
	<?php if (has_post_thumbnail( $post->ID ) ): ?>
	<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
		<div class="article-feature-image w-100 position-relative" style="background-image: url('<?php echo $image[0]; ?>')">
	  	</div>
	<?php endif; ?>
		<div class="article-content container">
			<div class="single-post-thumbnail-5 text-center"><h1 class="text-uppercase"><?php echo get_the_title($post->ID); ?></h1></div>
		<?php
			the_content();
		?>
		</div>
	</div>
</div>
<?php get_footer() ?>