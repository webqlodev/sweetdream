<?php
/*
	Template Name: warranty-template
*/
get_header();
?>
<div class="warranty-section container p-0">
	<div class="warranty-inner-section">
		<?php
			the_content();
		?>
		<a href="/warranty-registration-form" rel="warranty registration"><button type="button" class="get_warranty_button" data-toggle="modal" data-target="#warranty_form">Get my warranty<i class="fas fa-arrow-right"></i></button></a>
	</div>
</div>
<?php get_footer() ?>