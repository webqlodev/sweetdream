<?php
/*
	Template Name: legendary-series template
*/
get_header();
?>
<div id="legendary" class="product-body-section">
	<div class="product-series-header-section container-fluid p-0 d-block d-lg-none">
		<div class="product-series-header-bg-img"></div>
	</div>
	<div class="product-series-header-section container d-lg-block d-none px-0">
		<img src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/05/LegendarySeries_Main-Image.jpg" alt="LegendarySeries_Main-Image" class="lazy-load vc_single_image-img attachment-full is-loaded img-fluid w-100">
	</div>
	<div class="product-series-details-section container px-0">
		<div class="product-series-details-inner-section row">
			<div class="product-title col-12 text-center pb-5">
				<img src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/03/legendary-logo.png" alt="legendary-logo" class="lazy-load vc_single_image-img attachment-full is-loaded img-fluid">
				<p>Individual and Simply Stylish. Relaxing your mind and giving you an even sounder sleep.</p>
			</div>
			<div class="product-image col-12 d-block d-lg-none my-auto">
				<img src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/05/LegendarySeries_01_YY_ProductImage.jpg" alt="LegendarySeries_01_YY_ProductImage" class="lazy-load w-100 vc_single_image-img attachment-full is-loaded img-fluid">
			</div>
			<div class="product-series-more-details col-lg-6 col-12 my-auto">
				<img src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/03/legendary-yy.png" alt="legendary-yy" class="lazy-load vc_single_image-img attachment-full is-loaded img-fluid">
				<ul class="text-left">
					<li>High quality soft and strong stretch knitted fabric.</li>
					<li>High density comfort layer to maximize your body pressure relieves.</li>
					<li>Bonnell springs system providing longlife support.</li>
					<li>Foambox give your mattress the best edge support.</li>
				</ul>
			</div>
			<div class="product-image col-lg-6 d-none d-lg-block my-auto">
				<img src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/05/LegendarySeries_01_YY_ProductImage.jpg" alt="LegendarySeries_01_YY_ProductImage" class="lazy-load w-100 vc_single_image-img attachment-full is-loaded img-fluid">
			</div>
			<div class="product-image col-12 col-lg-6 d-block d-lg-none my-auto">
				<img src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/05/LegendarySeries_02_SS_ProductImage.jpg" alt="LegendarySeries_02_SS_ProductImage" class="lazy-load w-100 vc_single_image-img attachment-full is-loaded img-fluid">
			</div>
			<div class="product-series-more-details col-lg-6 col-12 my-auto">
				<img src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/05/LegendarySeries_02_SS_Logo.png" alt="LegendarySeries_02_SS_Logo" class="lazy-load vc_single_image-img attachment-full is-loaded img-fluid">
				<ul class="text-left">
					<li>Premium imported fabric giving gentle soft and breathable holes allow for comfortable sleeping temperature.</li>
					<li>Double high density comfort layer conforms to your body, relieves pressure and helps you to relax.</li>
					<li>Individual pocketed spring coil support provides excellent postural support while eliminating partner disturbance.</li>
					<li>Foambox give your mattress the best edge support.</li>
				</ul>
			</div>
			<div class="product-image col-lg-6 d-none d-lg-block my-auto">
				<img src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/05/LegendarySeries_02_SS_ProductImage.jpg" alt="LegendarySeries_02_SS_ProductImage" class="lazy-load w-100 vc_single_image-img attachment-full is-loaded img-fluid">
			</div>
		</div>
	</div>
</div>
<?php get_footer() ?>