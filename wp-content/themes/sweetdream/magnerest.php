<?php
/*
	Template Name: magnerest-template
*/
get_header();
?>
<div id="magnerest" class="magnerest-section container-fluid p-0">
	<div class="magnerest-inner-section">
		<div class="container"><h1 class="col-12">magnerest <br>fabric</h1></div>
		<img src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/03/magnerest-picture-1.jpg" class="d-block d-lg-none w-100 lazy-load vc_single_image-img attachment-full is-loaded" alt="magnerest-picture-1">
		<div class="magnetic-field-section text-center container">
			<div class="row">
				<div class="col-lg-6 d-none d-lg-block finesse-image-section">
					<img src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/03/desktop-magnerest-picture-1.jpg" class="d-block w-100 lazy-load vc_single_image-img attachment-full is-loaded" alt="desktop-magnerest-picture-1">
				</div>
				<div class="col-12 col-lg-6 text-center finesse-text-section right-margin-0 ">
					<h3>Magnetic Field</h3>
					<p>The world is changing rapidly under the inﬂuence of technology and new living styles.When trying to catch the rhytm of life and embracing the spirit of time,our bodies are becoming more exhausted and our sleeping pattern is adversely affected by this situation. <span class="d-none d-lg-block"></p><p></span>The presence of the magnetic ﬁeld that surrounds us in space, just like gravity, is a proven scientiﬁc fact. The fact that the compasses do not point in the same direction in different places is merely one of the proofs of existence of this magnetic ﬁeld that ranges all the way from the earth to the sky. All living or non-living beings have their own magnetic ﬁelds and everything is under the inﬂuence of this magnetic ﬁeld called the “Magnetosphere” with its own magnetic ﬁeld. However, the effect of this magnetic ﬁeld is diminishing. <span class="d-none d-lg-block"></p><p></span>With the Magnerest matteress ticking developed by Boyteks, it is possible to avoid the effects of the diminishing magnetic ﬁeld deﬁciency and to render your life more productive by regaining the energy saved during sleep.</p>
				</div>
				<div class="col-12 d-block d-lg-none custom-padding finesse-image-section">
					<img src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/03/desktop-magnerest-picture-2.jpg" class="d-block w-100 lazy-load vc_single_image-img attachment-full is-loaded" alt="desktop-magnerest-picture-2">
				</div>
				<div class="col-12 col-lg-6 text-center finesse-text-section left-margin-0">
					<h3>The Magnerest Technology</h3>
					<p>The magnetic ﬁeld created by the Magnerest mattress ticking which is developed by Boyteks, passes through the tissue in waves and forms a current. The positive effects of the Magnerest mattress ticking have been tested by independent laboratories. According to test results, the mattress ticking is proven to improve sleep quality and increase the melatonin hormone.</p>
				</div>
				<div class="col-6 d-none d-lg-block finesse-image-section">
					<img src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/03/desktop-magnerest-picture-2.jpg" class="d-block w-100 lazy-load vc_single_image-img attachment-full is-loaded" alt="desktop-magnerest-picture-2">
				</div>
			</div>
			<div class="youtube-section container embed-responsive embed-responsive-16by9 p-0 d-lg-block d-none">
				<iframe class="embed-responsive-item lazy-load" src="https://www.youtube.com/embed/3lLO58SsZAE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
			</div>
			<div class="youtube-mobile-section container-fluid embed-responsive embed-responsive-16by9 p-0 d-block d-lg-none">
				<iframe class="embed-responsive-item lazy-load" src="https://www.youtube.com/embed/3lLO58SsZAE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
			</div>
		</div>	
		<div class="magnerest-advantages-section text-center container">
			<div class="row">
				<div class="d-none d-lg-block my-auto col-lg-6 finesse-image-section">
					<img src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/03/picture-3.jpg" class="d-block w-100 lazy-load vc_single_image-img attachment-full is-loaded" alt="magnerest-picture-3">
				</div>
				<div class="col-12 col-lg-6 finesse-text-section">
					<div class="advantages_section">
						<h3>Advantages of the <br>Magnerest Mattress Ticking</h3>
						<ul>
							<li>Has a permanent magnetic feature.</li>
							<li>It is an alternative therapy method.</li>
							<li>Washable.</li>
							<li>Enhances melatonin(sleep) hormone.</li>
							<li>Helps to decrease the cortisol(stress) hormone.</li>
							<li>Helps to prolong the deep sleep duration.</li>
							<li>Shortens the duration fall asleep.</li>
							<li>Helps to relieve muscle and joint pain.</li>
							<li>Ensures to wake up and start the day energetic.</li>
						</ul>
					</div>
				</div>
				<div class="d-lg-none d-block col-12 custom-padding finesse-image-section">
					<img src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/03/picture-3.jpg" class="d-block w-100 lazy-load vc_single_image-img attachment-full is-loaded" alt="magnerest-picture-3">
				</div>
			</div>
		</div>
	</div>
</div>
<?php get_footer() ?>
