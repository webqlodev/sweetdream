<?php
/*
	Template Name: contact-us-template
*/
get_header();
?>
<div class="contact-section container">
	<div class="contact-inner-section">
		<?php
			the_content();
		?>
	</div>
</div>
<?php get_footer() ?>