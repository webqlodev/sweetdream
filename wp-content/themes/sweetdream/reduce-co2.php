<?php
/*
	Template Name: reduceCO2-page-template
*/
get_header();
?>
<div class="reduceCO2-section container p-0">
	<div class="reduceCO2-inner-section">
		<div class="reduceCO2-content row">
			<div class="col-12 col-lg-6 pad-bot-25">
				<img src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/04/logo.png" class="lazy-load vc_single_image-img attachment-full is-loaded w-50 pad-bot-25" alt="reduce_co2-logo">
				<h2 class="pad-bot-25">We Promise Reduced Carbon Footprint</h2>
				<p class="pad-bot-25">
					We voluntarily calculated our carbon dioxide emissions as an empirical approach to reduce the green house gas emissions produced during our manufacturing processes. At the present time, there are no government or legislative requirements to calculate or measure emissions, but as a pro-active producer of household mattresses, SweetDream constvantly strives to protect and preserve our environment all while providing you the perfect sleep with the best of materials.
				</p>
				<h3 class="pad-bot-25">Stand for Less CO2</h3>
				<div class="pad-bot-25 d-flex">
					<img src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/04/foot.jpg" class="lazy-load vc_single_image-img attachment-full is-loaded pr-3 mb-auto" alt="foot">
					<span>
						<h5>What is an organization carbon footprint?</h5>
						<p>An organizational carbon footprint is the total amount of greenhouse gases emitted by an entire company over the course of one year. It is usually measured in tonnes of carbon dioxide.</p>
					</span>
				</div>
				<div class="d-flex">
					<img src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/04/foot.jpg" class="lazy-load vc_single_image-img attachment-full is-loaded pr-3 mb-auto" alt="foot">
					<span>
						<h5>What is greenhouse gas (GHG) and the effects on earth?</h5>
						<p>Greenhouse gases are gases in an atmosphere that absorb and emit radiation within the thermal infrared range. This process is the fundamental cause of the greenhouse effect / global warming</p>
					</span>
				</div>
			</div>
			<div id="reduce_co2" class="col-12 col-lg-6 m-auto">
				<img src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/04/co2.jpg" class="d-block w-100 m-auto lazy-load vc_single_image-img attachment-full is-loaded" alt="CO2">
			</div>
		</div>
	</div>
</div>
<?php get_footer() ?>