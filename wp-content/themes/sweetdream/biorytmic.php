<?php
/*
	Template Name: Biorytmic-template
*/
get_header();
?>
<div id="biorytmic" class="biorytmic-section container-fluid p-0">
	<div class="biorytmic-inner-section">
		<div class="container"><h1 class="col-12">biorytmic <br>fabric</h1></div>
		<img src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/03/biorytmic-picture-1.jpg" class="d-block d-lg-none w-100 lazy-load vc_single_image-img attachment-full is-loaded" alt="biorytmic-picture-1">
		<div class="biorytmic-field-section text-center container">
			<div class="row">
				<div class="finesse-image-section col-6 d-none d-lg-block">
					<img src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/03/finesse.jpg" class="d-block w-100 lazy-load vc_single_image-img attachment-full is-loaded" alt="finesse">
				</div>
				<div class="finesse-text-section col-12 col-lg-6">
					<h3>Our Essence</h3>
					<p>The human body operates through communication between tissues and organs, and when this intra-body communication is in harmony, we are balanced, joyous, and motivated. Noticeably, in the bustling society we live in today, there are constant obstacles which disrupt this communication. The workload grows heavier, the days grow longer, and the balance grows weaker.</p>
				</div>
				<div class="finesse-image-section col-12 d-block d-lg-none ">
					<img src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/03/biorytmic-picture-2.jpg" class="d-block w-100 lazy-load vc_single_image-img attachment-full is-loaded" alt="biorytmic-picture-2">
				</div>
				<div class="finesse-text-section col-12 col-lg-6">
					<h3>The Power of Minerals</h3>
					<p>In nature, minerals improve and inﬂuence the climate balance through their insulation effect. In biorytmic sleep, the same minerals are ﬁnely milled to particles to produce the same effect. Harmonizing the vibrations of the body's energy system to improve one's sense of balance.</p>
				</div>
				<div class="finesse-image-section col-6 d-none d-lg-block">
					<img src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/03/biorytmic-picture-2.jpg" class="d-block w-100 lazy-load vc_single_image-img attachment-full is-loaded" alt="biorytmic-picture-2">
				</div>
				<div class="finesse-image-section col-12 col-lg-6">
					<img src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/03/biorytmic-picture-3.jpg" class="d-block w-100 lazy-load vc_single_image-img attachment-full is-loaded" alt="biorytmic-picture-3">
				</div>
				<div class="finesse-text-section col-12 col-lg-6">
				<h3>Rejuvenation</h3>
					<p>Healing therapies such as meditation, acupuncture, and natural stone therapies have been developed solely for the purpose of restoring the body's natural biorhythm, leading to a harmonious intra-body communication, better vitality, and rejuvenation. Following the path, Biorytmic Sleep was developed with the goal of improving intra-body communication through the use of special minerals found in nature.</p>
				</div>
			</div>
			<div class="youtube-section container embed-responsive embed-responsive-16by9 p-0 d-lg-block d-none">
				<iframe class="embed-responsive-item lazy-load" src="https://www.youtube.com/embed/-rhK17NXKJ4" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
			</div>
			<div class="youtube-mobile-section container-fluid embed-responsive embed-responsive-16by9 p-0 d-block d-lg-none">
				<iframe class="embed-responsive-item lazy-load" src="https://www.youtube.com/embed/-rhK17NXKJ4" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
			</div>
		</div>
		
		<div class="biorhythm-bottom-section text-center container">
			<div class="row">
				<div class="finesse-text-section col-12">
					<h3>Biorytmic Sleep</h3>
					<p class="pad-bot-25">Experience the healing energy of natural minerals that regulate your body's natural biorhythm, relieve stress, and create the ideal sleeping environment. Through the simple process of sleep, you can now improve your intra-body communication which paves the way to better health and vitality,lesser stress, and a more balanced life. Communicate well inside to communicate excellent outside. Embedded into the mattress cover fabric, the Biorytmic Sleep properties completely imbue the mattress to allow you maximum rejuvenation and balance recovery.</p>
					<a href="/royal-blue-series" rel="royal-blue-series"><div class="learn_more_button">Find a Mattress with Biorytmic Fabric<i class="fas fa-arrow-right"></i></div></a>
				</div>
				<div class="finesse-image-section col-12 col-lg-6">
					<img src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/03/biorytmic-picture-4.png" class="d-block w-100 lazy-load vc_single_image-img attachment-full is-loaded" alt="biorytmic-picture-4">
				</div>
				<div class="finesse-text-section col-12 col-lg-6">
					<p class="pad-bot-25">Mechanical tests were conducted in Germany to investigate the effectiveness of Biorytmic Sleep compared to conventional sleep, which resulted in Biorytmic Sleep participants feeling more rejuvenated and exhibiting better balance and concentration.</p>
					<button type="button" class="learn_more_button" data-toggle="modal" data-target="#advantages_biorytmic">View result <i class="fas fa-arrow-right"></i></button>
				</div>
				<!-- Modal -->
				<div class="modal fade" id="advantages_biorytmic" tabindex="-1" role="dialog" aria-hidden="true">
				  <div class="modal-dialog" role="document">
				    <div class="modal-content">
				      <div class="modal-header">
				        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
				          <span aria-hidden="true">&times;</span>
				        </button>
				      </div>
				      <div class="modal-body">
				       	<div class="col-12 finesse-text-section px-0">
				       		<div class="advantages_section">
								<h3>Advantages of the <br>Biorytmic Sleep Mattress ticking</h3>
								<ul>
									<li>Has a permanent magnetic feature.</li>
									<li>It is an alternative therapy method.</li>
									<li>Enhances melatonin(sleep) hormone.</li>
									<li>Helps to decrease the cortisol(stress) hormone.</li>
									<li>Helps to prolong the deep sleep duration.</li>
									<li>Shortens the duration fall asleep.</li>
									<li>Helps to relieve muscle and joint pain.</li>
									<li>Ensures to wake up and start the day energetic.</li>
								</ul>
							</div>
						</div>
						<div class="col-12 biorhythm-test-section p-0 pb-4">
							<div class="row text-center">
								<div class="col-12"><h3>Test out the Biorytmic Sleep</h3></div>
								<div class="col-12 pb-3"><p class="text-left bold">The mechanical body balance tests are first carried out without the presence of Biorytmic Sleep mattress ticking. Later the same tests are carried out on persons who have been in contact with Biorytmic Sleep mattress ticking for at least 5 minutes. The same tests are repeated and the differences are observed.</p></div>
								<div class="col-md-4 col-12 pb-4">
									<img src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/04/circle-2.png" class="d-block lazy-load vc_single_image-img attachment-full is-loaded" alt="Circle">
									<p class="text-left"><span class="bold">Test 1:</span> Feet are opened shoulder length. Place hands behinds at waist length and join them palms facing upwards. Apply force downwards from the point of connection.</p>
								</div>
								<div class="col-md-4 col-12 pb-4">
									<img src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/04/Circle.png" class="d-block lazy-load vc_single_image-img attachment-full is-loaded" alt="Cricle-2">
									<p class="text-left"><span class="bold">Test 2:</span> The person performing the test stands with arms spread out and stands on one foot. The foot that is raised applies a certain degree of force on the arm.</p>
								</div>
								<div class="col-md-4 col-12 pb-4">
									<img src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/04/circle-3.png" class="d-block lazy-load vc_single_image-img attachment-full is-loaded" alt="circle2-3">
									<p class="text-left"><span class="bold">Test 3:</span> The person performing the test stands with two feets adjoined, hands by their side, palms facing upwards. A certain degree of force is applied with the palm.</p>
								</div>
							</div>
						</div>
				      </div>
				    </div>
				  </div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php get_footer() ?>
