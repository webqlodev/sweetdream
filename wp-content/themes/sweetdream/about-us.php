<?php
/*
	Template Name: aboutus-template
*/
get_header();
?>
<div class="body-section">
	<div class="about-us-top-section container-fluid p-0 about_bg_img">
		<div id="firefly" class="aboutus-mission-session container">
			<div class="aboutus-mission-inner-section">
				<sup class="custom_quote"><i class="fas fa-quote-left"></i></sup>
				<h4 class="mission_details pl-4">
					A Pioneering brand in sleep and bedding solutions, SweetDream's reputation and achievements were humbly rooted within the great aspirations and foresight of founder, Mr. Tan Yok Chin 41 years earlier.
					<sup><i class="fas fa-quote-right"></i></sup>
				</h4>
			</div>
		</div>
		<div class="aboutus-details-session container">
			<div class="aboutus-details-inner-section">
				<p>In 1978, Mr.Tan became the ﬁrst in Malaysia to import a spring coil machine from Switzerland and SweetDream Corporation Industrial Sdn. Bhd. was duly established to manufacture spring mattresses.</p><br>
				<p>Today, Malaysia's Number 1.Bedding Solutions brand has expanded its horizons above and beyond. With presence in over 20 countries across the globe, and with an extensive line of revolutionary bedding products, SweetDream has gained the reputation as a Premium International Brand.</p><br>
				<div class="d-lg-none d-block pb-4">
					<img src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/03/Member-of-Better-Sleep-Council.png" class="w-100 lazy-load vc_single_image-img attachment-full is-loaded" alt="Member-of-Better-Sleep-Council"><br>
				</div>
				<p>We are deeply committed to the quality and technological innovation of sleep.</p><br>
				<p>Building a multi-million Ringgit Research and Development centre is just a beginning.So is its substantial investment in Green technology and unwavering commitment to the environment-friendlycause.</p><br>
				<p>SweetDream is constantly raising the bar to catch up with its ever-growing aspirations and progressive inspirations. Its passion for sleeping solutions and dedication towards the environment makes SweetDream a truly extraordinary brand and a Climate Friendly Producer.</p><br>
				<div class="d-lg-none d-block">
					<img src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/03/sweetdream-logo.png" class="w-100 lazy-load vc_single_image-img attachment-full is-loaded" alt="sweetdream-logo"><br>
				</div>
			</div>
			<div class="aboutus-details-mobile-img-section d-none d-lg-block">
				<img src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/03/Member-of-Better-Sleep-Council.png" class="d-block w-100 lazy-load vc_single_image-img attachment-full is-loaded" alt="Member-of-Better-Sleep-Council">
				<img src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/03/sweetdream-logo.png" class="d-block w-100 lazy-load vc_single_image-img attachment-full is-loaded" alt="sweetdream-logo">
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
	<div class="about-us-bot-section container aboutus_white_bg">
		<div class="about-us-bot-inner-section text-center row">
			<div class="col-12 pad-bot-75 text-left producer-section">
				<h1>SWEETDREAM,</h1>
				<h3 class="pad-bot-25">Your Climate-Friendly Producer</h3>
				<p>In our pursuit to provide you with asleep unsurpassed by any others, we have researched, designed and implemented countless technologies, acquired international standards, brand recognition and distinctions, and cultivated ourselves to be the best ﬁt for you and for our earth. So, proudly....</p>
			</div>
			<h3 class="text-left col-12 pad-bot-25">WE ARE//</h3>
			<div class="row m-0 producer-detail-section pad-bot-75">
				<div class="col-12 col-lg-5 img-pad my-auto">
					<img src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/03/Mc-free.png" class="d-block m-auto lazy-load vc_single_image-img attachment-full is-loaded" alt="Mc-free">
				</div>
				<div class="col-12 col-lg-7">
					<h3 class="pad-bot-25">Free From Harmful Chemicals</h3>
					<p>Methylene Chloride(MC) is a chemical compound used in the foam manufacturing process. Its usage is banned in the US and Europe as MC is a suspected carcinogen but it is still widely used in Asia except at our SweetDream Manufacturing plant.</p><br>
				</div>
			</div>
			<h3 class="text-left col-12 pad-bot-25">WE PIONEERED//</h3>
			<div class="row m-0 producer-detail-section pad-bot-75">
				<div class="col-12 col-lg-5 img-pad my-auto">
					<img src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/03/BAS.png" class="d-block m-auto lazy-load vc_single_image-img attachment-full is-loaded" alt="BAS">
				</div>
				<div class="col-12 col-lg-7">
					<h3 class="pad-bot-25">The Body Architect System<sup>TM</sup></h3>
					<p>Body-ArchitecSystem<sup>TM</sup> is another innovation brought to you by SweetDream to help you achieve the most comfortable and healthy sleeping posture. Designed to protect your spine and reduce pressure points, it provides the perfect environment for your body to get the rest and recharge it deserves. The ergonomic design of SweetDream's Body-ArchitecSystem<sup>TM</sup> is created by perfor ing calculated incisions on the comfort layer to meet the unique requirements of different user groups.</p>
				</div>
			</div>
			<h3 class="text-left col-12 pad-bot-25">WE PROMISE//</h3>
			<div class="row m-0 producer-detail-section pad-bot-75">
				<div class="col-12 col-lg-5 img-pad my-auto">
					<img src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/04/logo.png" class="d-block m-auto lazy-load vc_single_image-img attachment-full is-loaded" alt="Reduced Carbon Footprint">
				</div>
				<div class="col-12 col-lg-7">
					<h3 class="pad-bot-25">Reduced Carbon Footprint</h3>
					<p>We voluntarily calculated our carbon dioxide emissions as an empirical approach to reduce the green house gas emissions produced during our manufacturing processes. At the present time, there are no government or legislative requirements to calculate or measure emissions, but as a pro-active producer of household mattresses, SweetDream constvantly strives to protect and preserve our environment all while providing you the perfect sleep with the best of materials.</p>
				</div>
			</div>
			<div class="row m-0 producer-detail-section pad-bot-75">
				<div class="col-12 col-lg-5 img-pad my-auto">
					<img src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/04/gogreen-logo.png" class="d-block m-auto lazy-load vc_single_image-img attachment-full is-loaded" alt="go green">
				</div>
				<div class="col-12 col-lg-7">
					<h3 class="pad-bot-25">Environmental Love (Go Green)</h3>
					<p>SweetDream is now on the path to conserving the environment, as an eort to preserve our environment for the future generations. We are consistently dedicated to enhance the superior quality of our products through the use of sustainable raw materials and environmentally friendly processes by Recycling all foam scraps, Discharging non toxic waste water, Reducing carbon footprints, Being Methylene Chloride (MC) free and Being ozone depleters free.</p>
				</div>
			</div>
			<div class="manufacturer-section col-12 pad-bot-75 text-left">
				<h1 class="pad-bot-25">OUR <br>MANUFACTURER</h1>
				<p class="pad-bot-25">SDIC is a leading manufacturer, marketer, and distributor of mattresses, we are conﬁdent to give you consistent product quality every time as we take pride in producing all our mattress components.</p>
				<img src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/05/Manufactur.jpg" class="d-block w-100 lazy-load vc_single_image-img attachment-full is-loaded" alt="manufacture">
			</div>
			<div class="col-12 text-center">
				<h3 class="pad-bot-25">Where will SweetDream go next?</h3>
				<img src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/05/Map.png" class="d-block w-100 lazy-load vc_single_image-img attachment-full is-loaded" alt="map">
			</div>
		</div>
	</div>
</div>
<?php get_footer() ?>