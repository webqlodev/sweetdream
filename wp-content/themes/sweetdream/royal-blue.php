<?php
/*
	Template Name: royal-blue-series template
*/
get_header();
?>
<div id="royal_blue" class="product-body-section">
	<div class="product-series-header-section container-fluid p-0 d-block d-lg-none">
		<div class="product-series-header-bg-img"></div>
	</div>
	<div class="product-series-header-section container d-lg-block d-none px-0">
		<img src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/03/rb-bg.jpg" alt="era-logo" class="lazy-load vc_single_image-img attachment-full w-100 is-loaded img-fluid">
	</div>
	<div class="product-series-details-section container px-0">
		<div class="product-series-details-inner-section row">
			<div class="product-title col-12 text-center pb-5">
				<img src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/03/Royal-blue-logo.png" alt="Royal-blue-logo" class="lazy-load vc_single_image-img attachment-full is-loaded img-fluid">
				<p>A new innovation in the world of sleep. Optimises recovery by improving intra-body communication during sleep, giving you heightened focus, maximized energy, and ultimate clarity during the day.</p>
			</div>
			<div class="product-image col-12 d-block d-lg-none my-auto">
				<img src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/03/rb-1.jpg" alt="rb-1" class="lazy-load w-100 vc_single_image-img attachment-full is-loaded img-fluid">
			</div>
			<div class="product-series-more-details col-lg-6 col-12 my-auto">
				<img src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/03/rb-logo.png" alt="rb-logo" class="lazy-load vc_single_image-img attachment-full is-loaded img-fluid">
				<ul class="text-left">
					<li>
						<h4>Individual Pocketed Spring</h4>
						<p>provide excellent body support and minimises partner disturbances.</p>
					</li>
					<li>
						<h4>Body Architec System<sup>TM</sup></h4>
						<p>cutting on comfort layer in accordance to human ergonomics (pressure relief).</p>
					</li>
					<li>
						<h4>Comfort Layer</h4>
						<p>double layer of VPF Rubbery Tech<sup>TM</sup>, soft ﬁllings adds support and comfort.</p>
					</li>
					<li>
						<h4>Non Flip Technology</h4>
						<p>ensure hassle-free usage as ﬂipping over of mattress is not required.</p>
					</li>
				</ul>
			</div>
			<div class="product-image col-lg-6 d-none d-lg-block my-auto">
				<img src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/03/rb-1.jpg" alt="rb-2" class="lazy-load w-100 vc_single_image-img attachment-full is-loaded img-fluid">
			</div>
			<div class="product-image col-12 d-block d-lg-none my-auto">
				<img src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/05/rb-2.jpg" alt="rb-2" class="lazy-load w-100 vc_single_image-img attachment-full is-loaded img-fluid">
			</div>
			<div class="product-series-more-details col-lg-6 col-12 my-auto">
				<img src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/03/rb-logo1.png" alt="rb-logo1" class="lazy-load vc_single_image-img attachment-full is-loaded img-fluid">
				<ul class="text-left">
					<li>
						<h4>Dual Spring</h4>
						<p>2.5” Mini Pocketed Spring as comfort padding that work independently and closely follow your body + Bonnell Springs provide all-over base support.</p>
					</li>
					<li>
						<h4>Body Architec System<sup>TM</sup></h4>
						<p>cutting on comfort layer in accordance to human ergonomics (pressure relief).</p>
					</li>
					<li>
						<h4>Comfort Layer</h4>
						<p>3” inch comfort layer of high density, durable and elastic VPF RubberyTech<sup>TM</sup> Foam.</p>
					</li>
					<li>
						<h4>Non Flip Technology</h4>
						<p>ensure hassle-free usage as ﬂipping over of mattress is not required.</p>
					</li>
				</ul>
			</div>
			<div class="product-image col-lg-6 d-none d-lg-block my-auto">
				<img src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/05/rb-2.jpg" alt="rb-2" class="lazy-load w-100 vc_single_image-img attachment-full is-loaded img-fluid">
			</div>
			<div class="product-image col-12 d-block d-lg-none my-auto">
				<img src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/03/rb-3.jpg" alt="rb-3" class="lazy-load w-100 vc_single_image-img attachment-full is-loaded img-fluid">
			</div>
			<div class="product-series-more-details col-lg-6 col-12 my-auto">
				<img src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/03/rb-logo2.png" alt="rb-logo2" class="lazy-load vc_single_image-img attachment-full is-loaded img-fluid">
				<ul class="text-left">
					<li>
						<h4>Double Layer Pocketed Spring</h4>
						<p>2.5” Mini Pocketed provide exceptional motion separation and zoning back support while conforming to your body’s unique shape. Base individual 7” Pocketed spring coil provides excellent postural support while eliminating partner disturbance.</p>
					</li>
					<li>
						<h4>Body Architec System<sup>TM</sup></h4>
						<p>cutting on comfort layer in accordance to human ergonomics (pressure relief).</p>
					</li>
					<li>
						<h4>Comfort Layer</h4>
						<p>3” inch comfort layer of high density, durable and elastic VPF RubberyTech<sup>TM</sup> Foam.</p>
					</li>
					<li>
						<h4>Non Flip Technology</h4>
						<p>ensure hassle-free usage as ﬂipping over of mattress is not required.</p>
					</li>
				</ul>
			</div>
			<div class="product-image col-lg-6 d-none d-lg-block my-auto">
				<img src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/03/rb-3.jpg" alt="rb-3" class="lazy-load w-100 vc_single_image-img attachment-full is-loaded img-fluid">
			</div>
		</div>
	</div>
</div>
<?php get_footer() ?>