<?php
/*
	Template Name: era-series template
*/
get_header();
?>
<div id="era" class="product-body-section">
	<div class="product-series-header-section container-fluid p-0 d-block d-lg-none">
		<div class="product-series-header-bg-img"></div>
	</div>
	<div class="product-series-header-section container d-lg-block d-none px-0">
		<img src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/05/desktop_era_series.jpg" alt="desktop_era_series" class="lazy-load w-100 vc_single_image-img attachment-full is-loaded img-fluid">
	</div>
	<div class="product-series-details-section container px-0">
		<div class="product-series-details-inner-section row">
			<div class="product-title col-12 text-center pb-5">
				<img src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/03/ERA-logo.png" alt="era-logo" class="lazy-load vc_single_image-img attachment-full is-loaded img-fluid">
				<p>Design with your Spine in Mind. The advanced spring system provide pressure point relief and spinal alignment.</p>
			</div>
			<div class="product-image col-12 d-block d-lg-none">
				<img src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/05/Era-Picture-1.png" alt="Era-Picture-1" class="lazy-load w-100 vc_single_image-img attachment-full is-loaded img-fluid">
			</div>
			<div class="product-series-more-details col-lg-6 col-12">
				<img src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/03/Era-logo1.png" alt="Era-logo1" class="lazy-load vc_single_image-img attachment-full is-loaded img-fluid">
				<h3>era pocket spring</h3>
				<ul class="text-left">
					<li>Quality german made knitted fabric</li>
					<li>Anti-dust mite protection</li>
					<li>High tensile 2.00mm carbon steel individual pocketed spring coil support provides excellent postural support eliminationg partner disturbance</li>
					<li>High density VPF Rubbery-Tech<sup>TM</sup> for enhanced durability and elasticity</li>
					<li>Quality assured according to ASTM International standards</li>
					<li>Distinguished mattress embroidery using advanced Japanese technology</li>
					<li>REACH & Certipur-US compliant</li>
					<li>Non-ﬂip technology</li>
				</ul>
			</div>
			<div class="product-image col-lg-6 my-auto d-none d-lg-block">
				<img src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/05/Era-Picture-1.png" alt="Era-Picture-1" class="lazy-load w-100 vc_single_image-img attachment-full is-loaded img-fluid">
			</div>
			<div class="product-image col-12 d-block d-lg-none my-auto">
				<img src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/05/Era-Picture-2.jpg" alt="Era-Picture-2" class="lazy-load w-100 vc_single_image-img attachment-full is-loaded img-fluid">
			</div>
			<div class="product-series-more-details col-lg-6 col-12 my-auto">
				<img src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/05/Era-logo2.png" alt="Era-logo2" class="lazy-load vc_single_image-img attachment-full is-loaded img-fluid">
				<h3>era bonnell spring</h3>
				<ul class="text-left">
					<li>Quality german made knitted fabric</li>
					<li>Anti-dust mite protection</li>
					<li>Bonnell spring spinal care system made from high tensile 2.4mm carbon steel springs</li>
					<li>High density VPF Rubbery-Tech<sup>TM</sup> for enhanced durability and elasticity</li>
					<li>Quality assured according to ASTM International standards</li>
					<li>Distinguished mattress embroidery using advanced Japanese technology</li>
					<li>REACH & Certipur-US compliant</li>
					<li>Non-ﬂip technology</li>
				</ul>
			</div>
			<div class="product-image col-lg-6 my-auto d-none d-lg-block">
				<img src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/05/Era-Picture-2.jpg" alt="Era-Picture-2" class="lazy-load w-100 vc_single_image-img attachment-full is-loaded img-fluid">
			</div>
		</div>
	</div>
</div>
<?php get_footer() ?>