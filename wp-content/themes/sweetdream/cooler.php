<?php
/*
	Template Name: cooler-template
*/
get_header();
?>
<div class="cooler-section container">
	<div class="cooler-inner-section row">
		<div class="col-12 p-0 cooler-logo-section">
			<h1>cooler <br>fabric</h1>
		</div>
		<div class="col-12 d-lg-none d-block cooler-img-section p-0">
			<div class="row">
				<div class="col-12 pad-bot-25">
					<img src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/04/cooler-img-2.jpg" class="lazy-load vc_single_image-img attachment-full is-loaded" alt="cooler-img-2">
				</div>
				<div class="col-6 pr-2">
					<img src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/05/Cooler-1.jpg" class="lazy-load vc_single_image-img attachment-full is-loaded" alt="Cooler-1">
				</div>
				<div class="col-6 pl-2">
					<img src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/05/Cooler-2.jpg" class="lazy-load vc_single_image-img attachment-full is-loaded" alt="Cooler-2">
				</div>
			</div>
		</div>
		<div class="col-lg-6 col-12 cooler-content-section text-left">
			<h3>Deep Sleep on a Cool Bed!</h3>
			<p>
				Sleep… the essential for a healthy life. Considering we spend a third of our lives asleep, the importance of sleep is all the more apparent. Staying cool is an important component of good sleep.  
				<br>A certain drop in body temperature is to be expected before falling asleep. High ambient temperature makes it difﬁcult for the body to reduce its temperature. It’s difﬁcult for the body to reduce Its temperature when the ambient temperature is high, making it difﬁcult to fall asleep. Reduced body temperature makes it easy to fall asleep and improves sleep quality. That’s why sleep technology leader SweetDream developed Cooler mattress ticking.
			</p>
			<div class="row m-0">
				<div class="col-6 p-0">
					<img src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/04/cooler-img.jpg" class="w-100 lazy-load vc_single_image-img attachment-full is-loaded" alt="cooler-img">
				</div>
				<div class="col-6 p-0">
					<img src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/04/cooler-img-1.jpg" class="w-100 lazy-load vc_single_image-img attachment-full is-loaded" alt="cooler-img-1">
				</div>
			</div>
			
			<p>
				Cooler mattress ticking provides a sense of cool that helps the body stay cool. Cooler maximizes sleep quality by providing a relaxing sleep environment. Feel refreshed, and fall asleep quicker with the extra comfortable sleep environment provided by Cooler mattress ticking.
			</p>
			<h3>Beneﬁts of Cooler</h3>
			<div class="benefits_cooler row">
				<div class="col-4 text-center">
					<img src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/04/icon.png" class="lazy-load vc_single_image-img attachment-full is-loaded" alt="icon">
					<p>Exclusively Sweet Dream</p>
				</div>
				<div class="col-4 text-center">
					<img src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/04/icon1.png" class="lazy-load vc_single_image-img attachment-full is-loaded" alt="icon1">
					<p>Natural</p>
				</div>
				<div class="col-4 text-center">
					<img src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/04/icon2.png" class="lazy-load vc_single_image-img attachment-full is-loaded" alt="icon2">
					<p>Test Reports</p>
				</div>
				<div class="col-4 text-center">
					<img src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/04/icon3.png" class="lazy-load vc_single_image-img attachment-full is-loaded" alt="icon3">
					<p>Air Permeability</p>
				</div>
				<div class="col-4 text-center">
					<img src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/04/icon4.png" class="lazy-load vc_single_image-img attachment-full is-loaded" alt="icon4">
					<p>Permanent Feature</p>
				</div>
				<div class="col-4 text-center">
					<img src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/04/icon5.png" class="lazy-load vc_single_image-img attachment-full is-loaded" alt="icon5">
					<p>Comfort</p>
				</div>
			</div>
		</div>
		<div class="col-6 d-none d-lg-block cooler-img-section pr-0">
			<div class="row">
				<div class="col-12 pad-bot-25">
					<img src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/04/cooler-img-2.jpg" class="lazy-load vc_single_image-img attachment-full is-loaded" alt="cooler-img-2">
				</div>
				<div class="col-6 pr-2">
					<img src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/05/Cooler-1.jpg" class="lazy-load vc_single_image-img attachment-full is-loaded" alt="Cooler-1">
				</div>
				<div class="col-6 pl-2">
					<img src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/05/Cooler-2.jpg" class="lazy-load vc_single_image-img attachment-full is-loaded" alt="Cooler-2">
				</div>
			</div>
		</div>
		<div class="col-12 cooler-video-section">
			<div class="youtube-section container embed-responsive embed-responsive-16by9 p-0 d-lg-block d-none">
				<iframe class="embed-responsive-item lazy-load" src="https://www.youtube.com/embed/n2WlO6tu5NE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
			</div>
			<div class="youtube-mobile-section container-fluid embed-responsive embed-responsive-16by9 p-0 d-block d-lg-none">
				<iframe class="embed-responsive-item lazy-load" src="https://www.youtube.com/embed/n2WlO6tu5NE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
			</div>
		</div>
	</div>
</div>
<?php get_footer() ?>