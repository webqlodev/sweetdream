<?php
/*
	Template Name: Homepage-template
*/
get_header();
?>
<div class="body-section">
	<div class="title-section container-fluid p-0 position-relative">
		<div class="wrap_header_title">
			<div class="row title-inner-section">
				<div class="col-12 text-center">
					<img src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/03/logo.png" class="lazy-load vc_single_image-img attachment-full is-loaded">
					<p>Your No.1 Bedding Solutions Brand</p>
				</div>
			</div>
			<div class="scrolldown-section w-100">
				<div class="straight_line"></div>
				<div class="scrolldown">scroll down</div>
			</div>
		</div>
	</div>
	<div class="subtitle-section container p-md-0">
		<div class="subtitle-inner-section d-flex flex-row mt-0">
			<div class="subtitle-liner col-md-1 col-2 p-0">01</div>
			<div class="pl-3">
				<h1 class="text-uppercase">series</h1>
				<p>Find your comfort zone</p>
			</div>
		</div>
	</div>
	<div class="series-section container-fluid d-lg-none d-block p-0">
		<div id="series_mobile_carousel" class="owl-carousel owl-theme">
			<div class="item">
				<a href="/royal-blue-series" rel="royal-blue-series">
	    		<div class="carousel_img">
		      		<img src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/03/royal-blue-mob.png" class="w-100" alt="royal_blue">
		      	</div>
		      	<div class="carousel_details">
		      		<div class="carousel_inner_details">
			      		<div class="series_logo_img"><img src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/03/Royal-blue-logo.png" alt="royal_blue_logo" class="m-auto"></div>
			      		<p class="series_description">A new innovation in the world of sleep. Optimises recovery by improving intra-body...</p>
			      		<div class="learn_more_button">Learn more <i class="fas fa-arrow-right"></i></div>
			      	</div>		
		      	</div>
		      	</a>
		    </div>
		    <div class="item">
		    	<a href="/era-series" rel="era-series">
		    	<div class="carousel_img">
		      		<img src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/05/mobile_era_series.jpg" class="w-100" alt="mobile_era_series">
		      	</div>
		      	<div class="carousel_details">
		      		<div class="carousel_inner_details">
			      		<div class="series_logo_img"><img src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/03/ERA-logo.png" alt="ERA-logo" class="m-auto"></div>
			      		<p class="series_description">Design with your Spine in Mind. The advanced spring system provide pressure point relief......</p>
			      		<div class="learn_more_button">Learn more <i class="fas fa-arrow-right"></i></div>
			      	</div>
		      	</div>
		      	</a>
		    </div>
		    <div class="item">
		    	<a href="/legendary-series" rel="legendary-series">
		    	<div class="carousel_img">
		      		<img src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/03/legendary.jpg" class="w-100" alt="era">
		      	</div>
		      	<div class="carousel_details">
		      		<div class="carousel_inner_details">
			      		<div class="series_logo_img"><img src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/03/legendary-logo.png" alt="legendary-logo" class="m-auto"></div>
			      		<p class="series_description">Individual and Simply Stylish. Relaxing your mind and giving you an even sounder sleep.</p>
			      		<div class="learn_more_button">Learn more <i class="fas fa-arrow-right"></i></div>
			      	</div>
		      	</div>
		      	</a>
		    </div>
		    <div class="item">
		    	<a href="/explorer-series" rel="explorer-series">
		    	<div class="carousel_img">
		      		<img src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/03/together.jpg" class="w-100" alt="together">
		      	</div>
		      	<div class="carousel_details">
		      		<div class="carousel_inner_details">
			      		<div class="series_logo_img"><img src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/05/ExplorerSeries_Logo.png" alt="ExplorerSeries_Logo" class="m-auto"></div>
			      		<p class="series_description">The perfect combination for a SweetDream. Extra comfort and plush sleeping experience......</p>
			      		<div class="learn_more_button">Learn more <i class="fas fa-arrow-right"></i></div>
			      	</div>
		      	</div>
		      	</a>
		    </div>
		</div>
	</div>
	<div class="series-section container d-none d-lg-block p-0">
		<div id="series_carousel" class="owl-carousel owl-theme">
			<div class="col-12 item">
		  		<div class="row">
		  			<div class="offset-1 col-6 pl-3 carousel_img">
			      		<img src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/03/Desktop-series-product-pic-2.jpg" class="w-100 lazy-load vc_single_image-img attachment-full is-loaded" alt="royal_blue">
			      	</div>
			      	<div class="col-4 carousel_details">
			      		<div class="series_logo_img"><img src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/03/Royal-blue-logo.png" alt="royal_blue_logo" class="lazy-load vc_single_image-img attachment-full is-loaded img-fluid"></div>
			      		<p class="series_description">A new innovation in the world of sleep. Optimises recovery by improving intra-body...</p>
			      		<a href="/royal-blue-series" rel="royal-blue-series"><div class="learn_more_button">Learn more <i class="fas fa-arrow-right"></i></div></a>
			      	</div>
		  		</div>
		  	</div>
		  	<div class="col-12 item">
		  		<div class="row">
		  			<div class="offset-1 col-6 pl-3 carousel_img">
			      		<img src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/05/desktop_era_series.jpg" class="lazy-load vc_single_image-img attachment-full is-loaded w-100" alt="desktop_era_series">
			      	</div>
			      	<div class="col-4 carousel_details">
			      		<div class="series_logo_img"><img src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/03/ERA-logo.png" alt="ERA-logo" class="lazy-load vc_single_image-img attachment-full is-loaded img-fluid"></div>
			      		<p class="series_description">Design with your Spine in Mind. The advanced spring system provide pressure point relief......</p>
			      		<a href="/era-series" rel="era-series"><div class="learn_more_button">Learn more <i class="fas fa-arrow-right"></i></div></a>
			      	</div>
		  		</div>
	  		</div>
	  		<div class="col-12 item">
		  		<div class="row">
		  			<div class="offset-1 col-6 pl-3 carousel_img">
			      		<img src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/05/LegendarySeries_Main-Image.jpg" class="lazy-load vc_single_image-img attachment-full is-loaded w-100" alt="LegendarySeries_Main-Image">
			      	</div>
			      	<div class="col-4 carousel_details">
			      		<div class="series_logo_img"><img src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/03/legendary-logo.png" alt="legendary-logo" class="lazy-load vc_single_image-img attachment-full is-loaded img-fluid"></div>
			      		<p class="series_description">Individual and Simply Stylish. Relaxing your mind and giving you an even sounder sleep.</p>
			      		<a href="/legendary-series" rel="legendary-series"><div class="learn_more_button">Learn more <i class="fas fa-arrow-right"></i></div></a>
			      	</div>
		  		</div>
		  	</div>
		  	<div class="col-12 item">
		  		<div class="row">
		  			<div class="offset-1 col-6 pl-3 carousel_img">
			      		<img src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/05/desktop_explorer_Series.jpg" class="lazy-load vc_single_image-img attachment-full is-loaded w-100" alt="desktop_explorer_Series">
			      	</div>
			      	<div class="col-4 carousel_details">
			      		<div class="series_logo_img"><img src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/05/ExplorerSeries_Logo.png" alt="ExplorerSeries_Logo" class="lazy-load vc_single_image-img attachment-full is-loaded img-fluid"></div>
			      		<p class="series_description">The perfect combination for a SweetDream. Extra comfort and plush sleeping experience......</p>
			      		<a href="/explorer-series" rel="explorer-series"><div class="learn_more_button">Learn more <i class="fas fa-arrow-right"></i></div></a>
			      	</div>
		  		</div>
		  	</div>
		</div>
	</div>
	<div id="finesse" class="subtitle-section container p-md-0">
		<div class="subtitle-inner-section d-flex flex-row">
			<div class="subtitle-liner col-md-1 col-2 p-0">02</div>
			<div class="pl-3">
				<h1 class="text-uppercase">finesse</h1>
			</div>
		</div> 
	</div>
	<div class="finesse-section container d-none d-lg-block p-0">
		<div class="finesse-inner-section">
			<div class="biorytmic-section col-12 p-0">
				<div class="biorytmic-inner-section row ">
					<div class="finesse-left-section col-6">
						<img src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/03/finesse.jpg" class="d-block w-100 lazy-load vc_single_image-img attachment-full is-loaded" alt="finesse">
					</div>
					<div class="finesse-right-section col-6">
						<div class="row">
							<div class="finesse-details col-12">
								<div class="limit-width">
									<div class="finesse_title">biorytmic <br>fabric</div>
						      		<p class="finesse_description">A new form of meditation, motion therapy and acupuncture called sleep.</p>
						      		<a href="/biorytmic-fabric" rel="biorytmic-fabric"><div class="learn_more_button">Learn more <i class="fas fa-arrow-right"></i></div></a>
					      		</div>
							</div>
							<div class="finesse-video col-12 text-center">
								<div class="embed-responsive embed-responsive-16by9">
									<iframe class="embed-responsive-item lazy-load" src="https://www.youtube.com/embed/-rhK17NXKJ4" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
								</div>
							</div>
						</div>
					</div>
				</div>	
			</div>
			<div class="magnerest-section col-12 p-0">
				<div class="magnerest-inner-section row">
					<div class="col-6 my-auto">
						<div class="finesse-details">
							<div class="finesse_title">magnerest <br>fabric</div>
				      		<p class="finesse_description">The human body operates through communication between tissues and organs, and when</p>
				      		<a href="/magnerest-fabric" rel="magnerest-fabric"><div class="learn_more_button">Learn more <i class="fas fa-arrow-right"></i></div></a>
						</div>
					</div>
					<div class="col-6">
						<img src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/03/magnerest-picture-1.jpg" class="d-block w-100 lazy-load vc_single_image-img attachment-full is-loaded" alt="magnerest-picture-1">
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="finesse-mobile-section container-fluid p-0 d-lg-none d-block">
		<div class="finesse-mobile-inner-section">
			<div class="container p-md-0">
				<div class="finesse-details">
					<div class="finesse_title">biorytmic <br>fabric</div>
		      		<p class="finesse_description">A new form of meditation, motion therapy and acupuncture called sleep.</p>
		      		<a href="/biorytmic-fabric" rel="biorytmic-fabric"><div class="learn_more_button">Learn more <i class="fas fa-arrow-right"></i></div></a>
				</div>
			</div>
			<div class="col-12 p-0 pb-3">
				<img src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/03/finesse.jpg" class="d-block w-100  lazy-load vc_single_image-img attachment-full is-loaded" alt="finesse">
			</div>
			<div class="col-12 p-0 pb-3">
				<div class="embed-responsive embed-responsive-16by9">
					<iframe class="embed-responsive-item lazy-load" src="https://www.youtube.com/embed/-rhK17NXKJ4" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
				</div>
			</div>
			<div class="container p-md-0 pt-3">
				<div class="finesse-details">
					<div class="finesse_title">magnerest <br>fabric</div>
		      		<p class="finesse_description">The human body operates through communication between tissues and organs, and when</p>
		      		<a href="/magnerest-fabric" rel="magnerest-fabric"><div class="learn_more_button">Learn more <i class="fas fa-arrow-right"></i></div></a>
				</div>
			</div>
			<div class="col-12 p-0">
				<img src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/03/magnerest-picture-1.jpg" class="d-block w-100 lazy-load vc_single_image-img attachment-full is-loaded" alt="magnerest-picture-1">
			</div>
		</div>
	</div>
	<div id="mission" class="subtitle-section container p-md-0">
		<div class="subtitle-inner-section d-flex flex-row">
			<div class="subtitle-liner col-md-1 col-2 p-0">03</div>
			<div class="pl-3">
				<h1 class="text-uppercase">our <br>mission</h1>
			</div>
		</div> 
	</div>
	<div id="firefly" class="mission-section container-fluid p-0">
		<div class="mission-inner-section container p-md-0">
			<div class="offset-md-1 col-md-10 col-12 pl-md-3 p-0">
				<sup class="custom_quote"><i class="fas fa-quote-left"></i></sup>
				<h4 class="mission_details pl-4">
					A Pioneering brand in sleep and bedding solutions, SweetDream's reputation and achievements were humbly rooted within the great aspirations and foresight of founder, Mr. Tan Yok Chin 41 years earlier.
					<sup><i class="fas fa-quote-right"></i></sup>
				</h4>
				<a href="/about-sweetdream" rel="about-us">
					<div class="read_more_button pl-4">Read more <i class="fas fa-arrow-right"></i></div>
				</a>
			</div>
		</div>
	</div>
	<div id="client" class="subtitle-section container p-md-0">
		<div class="subtitle-inner-section d-flex flex-row">
			<div class="subtitle-liner col-md-1 col-2 p-0">04</div>
			<div class="pl-3">
				<h1 class="text-uppercase">our <br>client</h1>
			</div>
		</div> 
	</div>
	<div class="our-client-section container d-none d-lg-block p-0">
		<div class="our-client-inner-section">
			<?php for($i=1; $i<=20; $i++): ?>
				<?php if($i == 1 || $i == 6 || $i == 11 || $i == 16): ?>
				<div class="row">
					<div class="col text-center">
						<img src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/05/logo_<?php echo $i; ?>.png" class="lazy-load vc_single_image-img attachment-full is-loaded" alt="logo_<?php echo $i; ?>">
					</div>
				<?php elseif($i == 5 || $i == 10 || $i == 15 || $i == 20): ?>
					<div class="col text-center">
						<img src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/05/logo_<?php echo $i; ?>.png" class="lazy-load vc_single_image-img attachment-full is-loaded" alt="logo_<?php echo $i; ?>">
					</div>
				</div>
				<?php else:?>
				<div class="col text-center">
					<img src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/05/logo_<?php echo $i; ?>.png" class="lazy-load vc_single_image-img attachment-full is-loaded" alt="logo_<?php echo $i; ?>">
				</div>	
				<?php endif;?>
			<?php endfor; ?>
		</div>
	</div>
	<div class="our-client-mob-section container d-block d-lg-none p-0">
		<div class="our-client-mob-inner-section">
			<div id="our_client_carousel" class="owl-carousel owl-theme">
				<?php 
					$i = 0;
					do{ 
				?>
				<div class="item">
					<?php $i++;?>
					<img src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/05/logo_<?php echo $i; ?>.png" class="lazy-load vc_single_image-img attachment-full is-loaded" alt="logo_<?php echo $i; ?>">
					<?php $i++;?>					
					<img src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/05/logo_<?php echo $i; ?>.png" class="lazy-load vc_single_image-img attachment-full is-loaded" alt="logo_<?php echo $i; ?>">
				</div>
				<?php 
					}
					while($i < 19); 
				?>
			</div>
		</div>
	</div>
	<div id="articles" class="subtitle-section container p-md-0">
		<div class="subtitle-inner-section d-flex flex-row">
			<div class="subtitle-liner col-md-1 col-2 p-0">05</div>
			<div class="pl-3">
				<h1 class="text-uppercase">related <br>articles</h1>
			</div>
		</div> 
	</div>
	<div class="articles-homepage-section container">
		<div class="row">
			<?php 
				$args = array(
					'post_type'=> 'post',
					'orderby'    => 'ID',
					'post_status' => 'publish',
					'order'    => 'DESC',
					'posts_per_page' => -1
				);
				$result = new WP_Query( $args );
				if ( $result-> have_posts() ) : 
			?>
			<?php 
				while ( $result->have_posts() ) : $result->the_post(); 
					$url = get_permalink();
				    $title = get_the_title();
				    $image = get_the_post_thumbnail(get_the_ID(), 'full');
			?>
			<div class="col-lg-4 col-md-6 col-12 articles_details position-relative py-3">
				<a href="<?php echo $url ?>" rel="<?php echo $title; ?>">
				<div class="articles_img"><?php echo $image; ?></div>
				<div class="position-absolute fixed-bottom text-center pb-4">
		      		<p class="articles_title"><?php echo $title; ?></p>
		      		<div class="learn_more_button">View article <i class="fas fa-arrow-right"></i></div>
		      	</div>
		      	 </a>
	      	</div>    
			<?php endwhile; ?>
			<?php 
				endif; 
				wp_reset_postdata(); 
			?>
		</div>
	</div>
</div>

<?php get_footer() ?>