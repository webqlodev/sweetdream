var isMobile = {
    Android: function() {
        return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function() {
        return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function() {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    Opera: function() {
        return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function() {
        return navigator.userAgent.match(/IEMobile/i);
    },
    Tablet: function() {
        return navigator.userAgent.match(/(ipad|tablet|(android(?!.*mobile))|(windows(?!.*phone)(.*touch))|kindle|playbook|silk|(puffin(?!.*(IP|AP|WP))))/i);
    },
    any: function() {
        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
    }
};
jQuery(document).ready(function($) {
    $('.footer-inner-section .footer_links .finesse-title').click(function(){
        var current_class = $('.footer-inner-section .footer_links ul#finesse').attr('class');
        if( current_class == "collape-footer-menu active" ){
            $('.footer-inner-section .footer_links ul#finesse').removeClass('active');
            $(this).removeClass('active');
        }else{
            $('.footer-inner-section .footer_links ul#finesse').addClass('active');
            $(this).addClass('active');
        }
    });
    $('.footer-inner-section .footer_links .about-us-title').click(function(){
        var current_class = $('.footer-inner-section .footer_links ul#about-us').attr('class');
        if( current_class == "collape-footer-menu active" ){
            $('.footer-inner-section .footer_links ul#about-us').removeClass('active');
            $(this).removeClass('active');
        }else{
            $('.footer-inner-section .footer_links ul#about-us').addClass('active');
            $(this).addClass('active');
        }
    });
    $('.footer-inner-section .footer_links .buy-online-title').click(function(){
        var current_class = $('.footer-inner-section .footer_links ul#buy-online').attr('class');
        if( current_class == "collape-footer-menu active" ){
            $('.footer-inner-section .footer_links ul#buy-online').removeClass('active');
            $(this).removeClass('active');
        }else{
            $('.footer-inner-section .footer_links ul#buy-online').addClass('active');
            $(this).addClass('active');
        }
    });
    $(location).attr('href');
    var get_url_pathname = window.location.pathname;
    var pathname = get_url_pathname.replace(/\//g,'');
    var pathname = pathname.replace(/%20|-/g,' ');
    var white_menu_header_url = ["about sweetdream"];
    var check_array = white_menu_header_url.indexOf(pathname);
    if(check_array == -1 && pathname != ""){
        changeBlackHeader(pathname);
    }
    if(pathname == "" || pathname == "about sweetdream"){
        runFireFlyPlugin();
    }
    if(pathname == ""){
        var mob_owl = $('#series_mobile_carousel');
        var desk_owl = $('#series_carousel');
        var our_client_owl = $('#our_client_carousel');
        runOwlCarousel(mob_owl,desk_owl,our_client_owl);
    }
});
/*function hideHeader(){
    jQuery('.masthead').addClass('d-none');
    jQuery('.footer-section').addClass('d-none');
}*/
function changeBlackHeader(){  
    jQuery('header.header-bar ul#primary-menu').children().addClass('black-menu');
    jQuery('.branding a img:nth-child(1)').attr('src','/wp-content/uploads/2019/05/logo-black.png');
    jQuery('.branding a img:nth-child(1)').attr('srcset','/wp-content/uploads/2019/05/logo-black.png');
} 
function runFireFlyPlugin(){
    if( isMobile.any() ) {
        jQuery.firefly({
            total: 15,
            ofTop: 0,
            ofLeft: 0,
            on: 'div#firefly',
            twinkle: 0.2,
            minPixel: 1,
            maxPixel: 2,
            color: '#ffffff',
            namespace: 'jqueryFireFly',
            zIndex: Math.ceil(Math.random() * 20) - 1,
            borderRadius: '50%',
            _paused: false
        });
    }else{
        jQuery.firefly({
            total: 30,
            ofTop: 0,
            ofLeft: 0,
            on: 'div#firefly',
            twinkle: 0.2,
            minPixel: 1,
            maxPixel: 2,
            color: '#ffffff',
            namespace: 'jqueryFireFly',
            zIndex: Math.ceil(Math.random() * 20) - 1,
            borderRadius: '50%',
            _paused: false
        });
    }
}
function runOwlCarousel(mob_owl,desk_owl,our_client_owl){
    mob_owl.owlCarousel({
        items:1,
        lazyLoad:true,
        loop:true,
        autoplay:true,
        autoHeight:true
    });
    mob_owl.on('mousewheel', '.owl-stage', function (e) {
        if (e.deltaY>0) {
            owl.trigger('next.owl');
        } else {
            owl.trigger('prev.owl');
        }
        e.preventDefault();
    });
    desk_owl.owlCarousel({
        items:1,
        lazyLoad:true,
        loop:true,
        autoplay:true,
        dots: false,
        nav: true,
        autoplayHoverPause: true,
        navText: [
                    "<span class='sprite sprite-arrow-left'></span>",
                    '<span class="sprite sprite-arrow-right"></span>'
                ]
    });
    our_client_owl.owlCarousel({
        items:2,
        lazyLoad:true,
        loop:true,
        margin:10,
        nav:true,
        dots: false,
        autoplay:true,
        navText: [
                    "<span class='sprite sprite-arrow-left'></span>",
                    '<span class="sprite sprite-arrow-right"></span>'
                ]
    });
}