jQuery(document).ready(function($) {
    var section_width = $('.storyline-section').outerWidth();
    var get_element_stories = $('#stories');
    var get_total_element = $('.story-detail').length;
    var item = 1;
    if( isMobile.any() && !(isMobile.Tablet()) && section_width < 690 ){
        get_element_stories.owlCarousel({
            items: item,
            lazyLoad:true,
            loop:false,
            autoplay:false,
            autoHeight:true
        });
    }else{
        if( isMobile.Tablet() || section_width == 690 ){ 
            item = 2;
        }else{
            item = 3;
        }
        get_element_stories.owlCarousel({
            items:item,
            slideBy:2,
            lazyLoad:true,
            loop:false,
            nav:true,
            dots: false,
            autoplay:false,
            navText: [
                        "<span class='sprite sprite-arrow-left'></span>",
                        '<span class="sprite sprite-arrow-right"></span>'
                ]
        });  
    }
    for( var i=0; i<=get_total_element; i++){
        var get_all_images = $('#story-'+i+' .story-description').find('img');
        var get_all_paragraph = $('#story-'+i+' .story-description').find('p');
        if( get_all_images.length == 2 ){
            $('#story-'+i+' .story-description img').css({
                "width" : "50%",
                "float" : "left",
                "padding-left" : "2.5px",
                "padding-right" : "2.5px"
            });
        }else{
            $('#story-'+i+' .story-description img').css({
                "padding-left" : "5px",
                "padding-right" : "5px"
            });
        }
        if( get_all_paragraph.length == 2 ){
            $('#story-'+i+' .story-description p').css({
                "float" : "left",
                "width" : "50%",
                "padding-top" : "15px"
            });
        }else{
            $('#story-'+i+' .story-description p').css({
                "padding-top" : "15px"
            });
        }
    }
});