<?php
	global $wpdb;
	$get_all_stories = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."stories ORDER BY year");
?>
<div class="container">
	<h1>Manage Stories</h1><hr>
	<?php $scs = $_GET['success']; ?>
	<?php if(isset($scs) && !empty($scs)): ?>
	<div class="alert alert-success"><strong>Successful</strong> added</div>
	<?php endif; ?>
	<?php $sdel = $_GET['del_ok']; ?>
	<?php if(isset($sdel) && $sdel =='1'): ?>
	<div class="alert alert-danger"><strong>Successful</strong> deleted.</div>
	<?php endif; ?>
	<?php if(isset($_GET['edit']) && !empty($_GET['edit'])): ?>
	<?php $edit_data = $wpdb->get_row('SELECT * FROM '.$wpdb->prefix.'stories WHERE id='.$_GET['edit']); ?>
	<?php endif; ?>
	<form method="POST" action="<?php echo admin_url('admin-post.php'); ?>">
		<?php if(isset($edit_data->id) && !empty($edit_data->id)): ?>
			<input type="hidden" name="action" value="edit_story">
		<?php else: ?>
			<input type="hidden" name="action" value="create_story">
		<?php endif; ?>
	 	<div class="form-group">
		    <label for="year">Year:</label>
		    <input type="text" class="form-control" name="year" id="year" placeholder="example: 1946" <?php if(isset($edit_data->year)) echo 'value="'.$edit_data->year.'"'; ?>>
		</div>
		<div class="form-group">
		    <label for="status">Status:</label>
		    <select class="form-control" id="status" name="status">
		      <option value="1" <?php if($edit_data->status == '1') echo 'selected="selected"'; ?>>Yes</option>
		      <option value="0" <?php if($edit_data->status == '0') echo 'selected="selected"'; ?>>No</option>
    		</select>
		</div>
		<div class="form-group">
		    <label for="description">Description:</label>
		    <?php 
		    	if(isset($edit_data->description) && !empty($edit_data->description)):
                    $content = str_replace('\"','',$edit_data->description);
                else:
                    $content = '';
                endif;
		    	$settings = array( 
		    		'textarea_name' => 'description',
		    		'textarea_rows' => 5
		    	);
		    	$editor_id = 'description';
				wp_editor( $content, $editor_id, $settings );
			?>
		</div>
		<?php if(isset($edit_data->id)): ?>
			<input type="hidden" name="id_story" value="<?php echo $edit_data->id; ?>">
			<a href="<?php echo admin_url('admin.php?page=manage_stories'); ?>" class="btn btn-primary">Back</a>
			<button class="btn btn-success">Edit</button>
			<?php else: ?>
			<button class="btn btn-success">Create</button>
		<?php endif; ?>
	</form>
</div>
<div class="container py-4">
	<table class="table">
	  <thead>
	    <tr>
	      <th scope="col">Year</th>
	      <th scope="col">Description</th>
	      <th scope="col">Status</th>
	      <th scope="col">Action</th>
	    </tr>
	  </thead>
	  <tbody>
	  	<?php 
		if( !is_null($get_all_stories) ){
			foreach ($get_all_stories as $get_all_story ) {
		?>
	    <tr>
	      <td><?php echo $get_all_story->year; ?></td>
	      <td><?php echo str_replace('\"','',$get_all_story->description); ?></td>
		  <td>
		  	<?php
		  		if( $get_all_story->status == 1 ){
		  	?>
		  	<span class="bg-success text-white">Enable</span>
		    <?php }else{ ?>
		  	<span class="bg-danger text-white">Disable</span>
		    <?php } ?>
		  </td>
		  <td>
		  	<a href="<?php echo admin_url('admin.php?page=manage_stories&edit='.$get_all_story->id); ?>" class="btn btn-primary btn-xs">edit</a>
			<a class="btn btn-danger btndel btn-xs text-white" idel="<?php echo $get_all_story->id; ?>">delete</a>
		  </td>
	    </tr>
		<?php } ?>
		<?php }else{ ?>
			<tr>
		      <td colspan="3" class="bg-secondary text-white text-center">No stories yet</td>
		    </tr>
		<?php }?>
	  </tbody>
	</table>
</div>
<div class="modal fade" id="mod_schedule_del">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Confirm Delete</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			</div>
			<div class="modal-body">
				<p>Confirm delete this story?</p>
			</div>
			<form action="<?php echo admin_url('admin-post.php'); ?>" method="POST">
			<div class="modal-footer">
				<input type="hidden" name="action" value="delete_story">
				<input type="hidden" name="id_story" id="iddel_hide" value="">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
				<button type="submit" class="btn btn-primary">Confirm</button>
			</div>
			</form>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->


