<?php
function wpdocs_theme_name_scripts() {
	$page_id = get_the_ID();
	if( is_front_page() || $page_id == 24){
		wp_enqueue_script('firefly-script', get_stylesheet_directory_uri().'/js/jquery.firefly-0.7.min.js',array(),'',false);
	}
	wp_enqueue_script('sweetdream-custom-script', get_stylesheet_directory_uri().'/js/custom.js',array(),'',true);
	wp_enqueue_script('bootstrap-script', get_stylesheet_directory_uri().'/js/bootstrap-4.1.3/js/bootstrap.min.js',array(),'',true);
	wp_enqueue_script('OwlCarousel-js', get_stylesheet_directory_uri().'/js/OwlCarousel2-2.3.4/owl.carousel.min.js','','',false);
	wp_enqueue_style('boostrap', get_stylesheet_directory_uri().'/css/bootstrap-4.1.3/css/bootstrap.min.css',array(),'',false);
	wp_enqueue_style('OwlCarousel-css', get_stylesheet_directory_uri().'/css/OwlCarousel2-2.3.4/owl.carousel.min.css',array(),'',false);
	wp_enqueue_style('OwlCarouseldefault-css', get_stylesheet_directory_uri().'/css/OwlCarousel2-2.3.4/owl.theme.default.min.css',array(),'',false);
	wp_enqueue_style('sweetdream-custom-css', get_stylesheet_directory_uri().'/css/new_custom.css?ver=1.2',array(),'',false);

   	if( $page_id == 217 ){
		wp_enqueue_style('story-css', get_stylesheet_directory_uri().'/css/story.css?ver=1.3',array(),'',false);
		wp_enqueue_script('story-js', get_stylesheet_directory_uri().'/js/custom-story.js?ver=1.4',array(),'',false);
	}
}
add_action( 'wp_enqueue_scripts', 'wpdocs_theme_name_scripts',999,0);

function add_admin_style(){
	wp_enqueue_script('admin-bootstrap-script', get_stylesheet_directory_uri().'/js/bootstrap-4.1.3/js/bootstrap.min.js',array(),'',true);
	wp_enqueue_style('admin-bootstrap-css', get_stylesheet_directory_uri().'/css/bootstrap-4.1.3/css/bootstrap.min.css',array(),'',false);
	wp_enqueue_script('admin-custom-js', get_stylesheet_directory_uri().'/js/admin_custom.js',array(),'',false);
}
add_action('admin_enqueue_scripts', 'add_admin_style');
function manage_stories_admin_menu() {
 	global $team_page;
 	add_menu_page( 'Manage Stories', 'Manage Stories', 'edit_posts', 'manage_stories', 'manage_stories_handler', '', 6 ) ;
}
add_action( 'admin_menu', 'manage_stories_admin_menu' );
function manage_stories_handler() {
  include(get_stylesheet_directory().'/manage_stories.php');
}
function add_story(){
	global $wpdb;
	$data = array(
			'year' => $_REQUEST['year'],
			'description' => $_REQUEST['description'],
			'status' => $_REQUEST['status'],
			'created_at' => date('YmdHis')
		);
	$wpdb->insert($wpdb->prefix.'stories', $data);
	wp_redirect(admin_url('admin.php?page=manage_stories&success=1'));
}
add_action('admin_post_create_story', 'add_story');

function edit_story(){
	global $wpdb;
	$data = array(
			'year' => $_REQUEST['year'],
			'description' => $_REQUEST['description'],
			'status' => $_REQUEST['status'],
			'updated_at' => date('YmdHis')
		);
	$wpdb->update($wpdb->prefix.'stories', $data, array('id'=>$_REQUEST['id_story']));
	wp_redirect(admin_url('admin.php?page=manage_stories&success=1'));
}
add_action('admin_post_edit_story', 'edit_story');

function delete_story(){
	global $wpdb;
	$id = $_REQUEST['id_story'];
	$wpdb->delete($wpdb->prefix.'stories', compact('id'));
	wp_redirect(admin_url('admin.php?page=manage_stories&del_ok=1'));
}
add_action('admin_post_delete_story', 'delete_story');
?>