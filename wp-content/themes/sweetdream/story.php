<?php
/*
	Template Name: the-story-2-template
*/
get_header();
?>
<div class="story-section container">
	<div class="story-inner-section row">
		<h1 class="text-uppercase w-100">the story</h1>
		<div class="story-timeline-section">
			<div class="single-story-section clearfix">
				<h2 class="story-year text-center">
					1967
				</h2>
				<div class="story-line"></div>
				<div class="story-dot"></div>
				<div class="story-detail text-left">
					<p>Malaysia’s First polyurethane foam factory was established.</p>
				</div>
			</div>
			<div class="single-story-section clearfix">
				<h2 class="story-year text-center">
					1978
				</h2>
				<div class="story-line"></div>
				<div class="story-dot"></div>
				<div class="story-detail text-left clearfix">
					<img src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/04/story-1.jpg" alt="story-1" class="lazy-load vc_single_image-img w-50 attachment-full is-loaded img-fluid float-left d-inline-block px-2 pb-2">
					<img src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/04/story-2.jpg" alt="story-2" class="lazy-load vc_single_image-img w-50 attachment-full is-loaded img-fluid d-inline-block float-right px-2 pb-2">
					<p class="w-50 d-inline-block float-left px-2">SweetDream Industrial Corporation Sdn Bhd was established.</p>
					<p class="w-50 d-inline-block float-right px-2">The ﬁrst in Malaysia to import spring coil machine in Switzerland.</p>
				</div>
			</div>
			<div class="single-story-section clearfix">
				<h2 class="story-year text-center">
					1980
				</h2>
				<div class="story-line"></div>
				<div class="story-dot"></div>
				<div class="story-detail text-left">
					<p>Acquired a coconut husk factory. Factories start to process raw materials like natural coconut ﬁbre sheet, polyurethane foam and others were setup.</p>
				</div>
			</div>
			<div class="single-story-section clearfix">
				<h2 class="story-year text-center">
					1982
				</h2>
				<div class="story-line"></div>
				<div class="story-dot"></div>
				<div class="story-detail text-left">
					<p>Began internationally exporting our products. Proudly ﬁrst bedding manufacturer in Malaysia to accomplish this.</p>
				</div>
			</div>
			<div class="single-story-section clearfix">
				<h2 class="story-year text-center">
					1997
				</h2>
				<div class="story-line"></div>
				<div class="story-dot"></div>
				<div class="story-detail text-left">
					<img height="50" src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/04/story-3.png" alt="story-3" class="lazy-load vc_single_image-img w-50 attachment-full is-loaded img-fluid float-left d-inline-block px-2 pb-2">
					<img height="50" src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/04/story-4.png" alt="story-4" class="lazy-load vc_single_image-img w-50 attachment-full is-loaded img-fluid d-inline-block float-right px-2 pb-2">
					<div class="clearfix"></div>
					<p class="d-block w-100 px-2">The ﬁrst and only mattress manufacturer in region to produce Methylene Chloride free mattresses. Imported Variable Pressure Foaming (VPF) machine from Spain to produce MC free foam.</p>
				</div>
			</div>
			<div class="single-story-section clearfix">
				<h2 class="story-year text-center">
					1999
				</h2>
				<div class="story-line"></div>
				<div class="story-dot"></div>
				<div class="story-detail text-left d-lg-flex">
					<img src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/04/story-5.png" alt="story-5" class="d-flex lazy-load vc_single_image-img attachment-full is-loaded img-fluid d-md-block d-inline-block float-left px-2 pb-2">
					<p class="d-inline-block d-md-block float-left px-2 mt-lg-auto">Received the International Gold Staraward for Quality.</p>
				</div>
			</div>
			<div class="single-story-section clearfix">
				<h2 class="story-year text-center">
					2003-2005
				</h2>
				<div class="story-line"></div>
				<div class="story-dot"></div>
				<div class="story-detail text-left d-lg-flex">
					<img src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/04/story-6.png" alt="story-6" class="lazy-load vc_single_image-img attachment-full is-loaded img-fluid d-md-block d-inline-block float-left px-2 pb-2">
					<p class="d-inline-block d-md-block float-left px-2 mt-lg-auto">Received Super brands awards and World Quality Commitment Award.</p>
				</div>
			</div>
			<div class="single-story-section clearfix">
				<h2 class="story-year text-center clearfix">
					2006-2008
				</h2>
				<div class="story-line"></div>
				<div class="story-dot"></div>
				<div class="story-detail text-left d-lg-flex">
					<img src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/04/story-7.jpg" alt="story-7" class="lazy-load vc_single_image-img attachment-full is-loaded img-fluid d-md-block d-inline-block float-left px-2 pb-2">
					<p class="d-inline-block d-md-block float-left px-2 mt-lg-auto">We engaged world leading energy and enviromental consultancy ﬁrm, McKinnonand Clarke to quantify the carbon footprint of our operations as a result of energy used.</p>
				</div>
			</div>
			<div class="single-story-section clearfix">
				<h2 class="story-year text-center">
					2007
				</h2>
				<div class="story-line"></div>
				<div class="story-dot"></div>
				<div class="story-detail text-left">
					<img src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/04/story-8.jpg" alt="story-8" class="lazy-load vc_single_image-img attachment-full is-loaded img-fluid w-100 pb-2">
					<p class="w-100 text-left">Opening of the multi-million Ringgit SweetDream R&D center. Adoption of America Standard Testing Method (ASTM) to ensure stringent quality control for all our products.</p>
				</div>
			</div>
			<div class="single-story-section clearfix">
				<h2 class="story-year text-center">
					2007-2008
				</h2>
				<div class="story-line"></div>
				<div class="story-dot"></div>
				<div class="story-detail text-left">
					<img src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/04/story-9.jpg" alt="story-9" class="lazy-load vc_single_image-img attachment-full is-loaded img-fluid d-inline-block float-left px-2 pb-2">
					<img src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/04/story-11.png" alt="story-10" class="lazy-load vc_single_image-img attachment-full is-loaded img-fluid d-inline-block float-right px-2 pb-2">
					<div class="clearfix"></div>
					<p class="d-block w-100 px-2">Became the ﬁrst and only bedding company in Malaysia to import the OFS-HE3 Horizontal Contour Cutting Machine. A hi-tech, German engineered million dollar machine that we use to create our proprietary Body-Architec System<sup>TM</sup>.</p>
				</div>
			</div>
			<div class="single-story-section clearfix">
				<h2 class="story-year text-center">
					2009
				</h2>
				<div class="story-line"></div>
				<div class="story-dot"></div>
				<div class="story-detail text-left">
					<img src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/04/story-10.jpg" alt="story-10" class="lazy-load vc_single_image-img attachment-full is-loaded img-fluid w-100 pb-2">
					<p class="w-100 text-left">SweetDream introduced cutting-edge, Italian design label and an innovative industry leader in gel products, Technogel bedding to Malaysia.</p>
				</div>
			</div>
			<div class="single-story-section clearfix">
				<h2 class="story-year text-center">
					2013
				</h2>
				<div class="story-line"></div>
				<div class="story-dot"></div>
				<div class="story-detail text-center">
					<img src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/04/story-12.png" alt="story-12" class="lazy-load vc_single_image-img attachment-full is-loaded pb-2 m-auto">
					<p class="w-100 text-left">Developed and launched our My Mattress Number mobile app which accurately calculate each individual’s Support Factor Index in order to ﬁnd the perfect balance between support and weight distribution.</p>
				</div>
			</div>
		</div>
	</div>
</div>
<?php get_footer() ?>