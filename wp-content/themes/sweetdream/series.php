<?php
/*
	Template Name: series-template
*/
get_header();
?>
<div class="series-header-section container-fluid p-0">
	<div class="series-header-inner-section w-100">
		<div class="series-title text-center w-100">
			<h1>SERIES</h1>
			<p>Find your comfort zone</p>
		</div>
		<div class="series-scrolldown w-100">
			<div class="d-block d-md-none straight_line"></div>
			<div class="scrolldown">scroll down</div>
		</div>
	</div>
</div>
<a href="/royal-blue-series" rel="royal-blue-series">
	<div id="royal_blue" class="series-product-section container-fluid p-0">
		<div class="row">
			<div class="series-image-section col-6 p-0 d-none d-md-block">
				<img src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/03/Desktop-series-product-pic-2.jpg" alt="Desktop-series-product-pic-2" class="lazy-load vc_single_image-img w-100 attachment-full is-loaded img-fluid">
			</div>
			<div class="series-product-inner-section col-12 col-md-6 p-0 text-center">
				<div class="series_logo_img"><img src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/03/Royal-blue-logo.png" alt="royal_blue_logo" class="lazy-load vc_single_image-img attachment-full is-loaded img-fluid"></div>
				<div class="product-details">
				A new innovation in the world of sleep. Optimises recovery by improving intra-body......
				</div>
				<div class="learn_more_button">Learn more <i class="fas fa-arrow-right"></i></div>	
			</div>
		</div>
	</div>
</a>
<a href="/era-series" rel="era-series">
<div id="era" class="series-product-section container-fluid p-0">
	<div class="row">
		<div class="series-product-inner-section col-12 col-md-6 p-0 text-center">
			<div class="series_logo_img">
				<img src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/03/ERA-logo.png" alt="ERA-logo" class="lazy-load vc_single_image-img attachment-full is-loaded img-fluid">
			</div>
			<div class="product-details">
			Design with your Spine in Mind. The advanced spring system provide pressure point relief......
			</div>
			<div class="learn_more_button">Learn more <i class="fas fa-arrow-right"></i></div></a>
		</div>
		<div class="series-image-section col-6 p-0 d-none d-md-block">
			<img src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/05/desktop_era_series.jpg" alt="desktop_era_series" class="lazy-load vc_single_image-img w-100 attachment-full is-loaded img-fluid">
		</div>
	</div>
</div>
</a>
<a href="/legendary-series" rel="legendary-series">
<div id="legendary" class="series-product-section container-fluid p-0">
	<div class="row">
		<div class="series-image-section col-6 p-0 d-none d-md-block">
			<img src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/03/Desktop-series-product-pic-4.jpg" alt="Desktop-series-product-pic-4" class="lazy-load vc_single_image-img w-100 attachment-full is-loaded img-fluid">
		</div>
		<div class="series-product-inner-section col-12 col-md-6 p-0 text-center">
			<div class="series_logo_img">
				<img src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/03/legendary-logo.png" alt="legendary-logo" class="lazy-load vc_single_image-img attachment-full is-loaded img-fluid">
			</div>
			<div class="product-details">
			Individual and Simply Stylish. Relaxing your mind and giving you an even sounder sleep.
			</div>
			<div class="learn_more_button">Learn more <i class="fas fa-arrow-right"></i></div>
		</div>
	</div>
</div>
</a>
<a href="/explorer-series" rel="explorer-series">
<div id="together" class="series-product-section container-fluid p-0">
	<div class="row">
		<div class="series-product-inner-section col-12 col-md-6 p-0 text-center">
			<div class="series_logo_img">
				<img src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/05/ExplorerSeries_Logo.png" alt="ExplorerSeries_Logo" class="lazy-load vc_single_image-img attachment-full is-loaded img-fluid">
			</div>
			<div class="product-details">
			The perfect combination for a SweetDream. Extra comfort and plush sleeping experience......
			</div>
			<div class="learn_more_button">Learn more <i class="fas fa-arrow-right"></i></div>
		</div>
		<div class="series-image-section col-6 p-0 d-none d-md-block">
			<img src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/05/desktop_explorer_Series.jpg" alt="desktop_explorer_Series" class="lazy-load vc_single_image-img w-100 attachment-full is-loaded img-fluid">
		</div>
	</div>
</div>
</a>
<div id="sporst" class="series-product-section container-fluid p-0">
	<div class="row">
		<div class="series-image-section col-6 p-0 d-none d-md-block">
			<img src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/03/Desktop-series-product-pic-6.jpg" alt="Desktop-series-product-pic-6" class="lazy-load vc_single_image-img w-100 attachment-full is-loaded img-fluid">
		</div>
		<div class="series-product-inner-section col-12 col-md-6 p-0 text-center text-center">
			<div class="series_logo_img">
				<img src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/03/sporst.png" alt="sporst" class="lazy-load vc_single_image-img attachment-full is-loaded img-fluid">
			</div>
			<div class="product-details">
			We see Sleep and Rest as an all-natural performance enhancing wonder......
			</div>
			<div class="learn_more_button">Coming soon<i class="d-none fas fa-arrow-right"></i></div>
		</div>
	</div>
</div>
<?php get_footer() ?>