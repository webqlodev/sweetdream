<?php
/*
	Template Name: gogreen-page-template
*/
get_header();
?>
<div class="gogreen-section container p-0">
	<div class="gogreen-inner-section">
		<div class="gogreen-content row">
			<div class="col-12 col-lg-6">
				<img src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/04/gogreen-logo.png" class="lazy-load vc_single_image-img attachment-full is-loaded w-50 pad-bot-25" alt="gogreen-logo">
				<h2 class="pad-bot-25">We Promise Environmental Love (GoGreen)</h2>
				<h3 class="pad-bot-25">How do we do green?</h3>
				<p class="pad-bot-25">SweetDream is now on the path to conserving the environment, as an effort to preserve our environment for the future generations. We are consistently dedicated to enhance the superior quality of our products through the use of sustainable raw materials and environmentally friendly processes by:</p>
				<div class="pad-bot-15 d-flex">
					<img src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/04/leaf.png" class="lazy-load vc_single_image-img attachment-full is-loaded pr-2 mb-auto" alt="leaf">
					<span>Reducing emissions of volatile organic components (VOC)</span> 
				</div>
				<div class="pad-bot-15 d-flex">
					<img src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/04/leaf.png" class="lazy-load vc_single_image-img attachment-full is-loaded pr-2 mb-auto" alt="leaf">
					<span>Recycling all foam scraps</span>
				</div>
				<div class="pad-bot-15 d-flex">
					<img src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/04/leaf.png" class="lazy-load vc_single_image-img attachment-full is-loaded pr-2 mb-auto" alt="leaf">
					<span>Discharging non toxic waste water</span>
				</div>
				<div class="pad-bot-15 d-flex">
					<img src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/04/leaf.png" class="lazy-load vc_single_image-img attachment-full is-loaded pr-2 mb-auto" alt="leaf">
					<span>Reducing carbon footprints</span>
				</div>
				<div class="pad-bot-15 d-flex">
					<img src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/04/leaf.png" class="lazy-load vc_single_image-img attachment-full is-loaded pr-2 mb-auto" alt="leaf">
					<span>Being Methylene Chloride (MC) free</span>
				</div>
				<div class="pad-bot-25 d-flex">
					<img src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/04/leaf.png" class="lazy-load vc_single_image-img attachment-full is-loaded pr-2 mb-auto" alt="leaf">
					<span>Being ozone depleters free</span>
				</div>
				<h3 class="pad-bot-25">There are many reason for gping green beyond protecting the enviroment:</h3>
				<div class="pad-bot-15 d-flex">
					<img src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/04/bulb.png" class="lazy-load vc_single_image-img attachment-full is-loaded pr-2 mb-auto" alt="bulb">
					<span>To slow down climate change, global warming and formation of holes in the Ozone layer</span>
				</div>
				<div class="pad-bot-15 d-flex">
					<img src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/04/bulb.png" class="lazy-load vc_single_image-img attachment-full is-loaded pr-2 mb-auto" alt="bulb">
					<span>To ensure the bright future of our future generations</span>
				</div>
				<div class="pad-bot-15 d-flex">
					<img src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/04/bulb.png" class="lazy-load vc_single_image-img attachment-full is-loaded pr-2 mb-auto" alt="bulb">
					<span>To preserve our limited natural resources</span>
				</div>
				<div class="pad-bot-15 d-flex">
					<img src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/04/bulb.png" class="lazy-load vc_single_image-img attachment-full is-loaded pr-2 mb-auto" alt="bulb">
					<span>To reduce air, water and soil pollutions</span>
				</div>
			</div>
			<div id="gogreen" class="col-12 col-lg-6 m-auto">
				<img src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/04/Gogreen.jpg" class="d-block w-100 m-auto lazy-load vc_single_image-img attachment-full is-loaded" alt="Gogreen">
			</div>
		</div>
	</div>
</div>
<?php get_footer() ?>