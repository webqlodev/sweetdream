<?php
/*
	Template Name: together-series template
*/
get_header();
?>
<div id="together" class="product-body-section">
	<div class="product-series-header-section container-fluid p-0 d-block d-lg-none">
		<div class="product-series-header-bg-img"></div>
	</div>
	<div class="product-series-header-section container d-lg-block d-none px-0">
		<img src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/05/desktop_explorer_Series.jpg" alt="desktop_explorer_Series" class="lazy-load vc_single_image-img attachment-full is-loaded img-fluid w-100">
	</div>
	<div class="product-series-details-section container px-0">
		<div class="product-series-details-inner-section row">
			<div class="product-title col-12 text-center pb-5">
				<img src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/05/ExplorerSeries_Logo.png" alt="ExplorerSeries-logo" class="lazy-load vc_single_image-img attachment-full is-loaded img-fluid">
				<p>The perfect combination for a SweetDream. Extra comfort and plush sleeping experience with therapeutic support.</p>
			</div>
			<div class="product-image col-12 d-block d-lg-none my-auto">
				<img src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/05/ExplorerSeries_ProductsImage-01-together.jpg" alt="ExplorerSeries_ProductsImage-01-together" class="lazy-load w-100 vc_single_image-img attachment-full is-loaded img-fluid">
			</div>
			<div class="product-series-more-details col-lg-6 col-12 my-auto">
				<img src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/05/together.png" alt="together-logo" class="lazy-load vc_single_image-img attachment-full is-loaded img-fluid">
				<ul class="text-left">
					<li>High quality soft and strong stretch knitted fabric with trendy zebra pattern.</li>
					<li>3” inch high density comfort layer to give you extra comfort and maximise your body pressure relieves.</li>
					<li>Bonnell springs system to increase the support for a long-lasting performance.</li>
					<li>Foambox give your mattress the best long life support.</li>
				</ul>
			</div>
			<div class="product-image col-lg-6 my-auto d-none d-lg-block">
				<img src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/05/ExplorerSeries_ProductsImage-01-together.jpg" alt="ExplorerSeries_ProductsImage-01-together" class="lazy-load w-100 vc_single_image-img attachment-full is-loaded img-fluid">
			</div>
			<div class="product-image col-12 col-lg-6 d-block d-lg-none my-auto">
				<img src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/05/ExplorerSeries_ProductsImage-02-in-alliance.jpg" alt="ExplorerSeries_ProductsImage-02-in-alliance" class="lazy-load w-100 vc_single_image-img attachment-full is-loaded img-fluid">
			</div>
			<div class="product-series-more-details col-lg-6 col-12 my-auto">
				<img src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/05/in-alliance.png" alt="in-alliance-logo" class="lazy-load vc_single_image-img attachment-full is-loaded img-fluid">
				<ul class="text-left">
					<li>Imported premium quality knitted fabric, durable and strong stretch.</li>
					<li>Diamond shape quilting to giving mattress effectively fixed is evenly integrated and more comfortable to use.</li>
					<li>2” high density comfort layer to maximise your body pressure relieves.</li>
					<li>7cm mesh fabric around the edge allowing air to ventilation the topper and gives a very pleasant sleeping temperature.</li>
					<li>The pocket springs are individually wrapped to insulate movements so you don't disturb your bed partner.</li>
					<li>1” compact foam base support.</li>
				</ul>
			</div>
			<div class="product-image col-lg-6 my-auto d-none d-lg-block">
				<img src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/05/ExplorerSeries_ProductsImage-02-in-alliance.jpg" alt="ExplorerSeries_ProductsImage-02-in-alliance" class="lazy-load w-100 vc_single_image-img attachment-full is-loaded img-fluid">
			</div>
			<div class="product-image col-12 d-block d-lg-none my-auto">
				<img src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/05/ExplorerSeries_ProductsImage-03-as-one.jpg" alt="ExplorerSeries_ProductsImage-03-as-one" class="lazy-load w-100 vc_single_image-img attachment-full is-loaded img-fluid">
			</div>
			<div class="product-series-more-details col-lg-6 col-12 my-auto">
				<img src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/05/as-one.png" alt="as-one-logo" class="lazy-load vc_single_image-img attachment-full is-loaded img-fluid">
				<ul class="text-left">
					<li>Imported premium quality knitted fabric, durable and strong stretch.</li>
					<li>Diamond shape quilting to giving mattress effectively fixed is evenly integrated and more comfortable to use.</li>
					<li>1.5” high density comfort layer to maximise your body pressure relieves.</li>
					<li>Bonnell springs system to increase the support for a long-lasting performance.</li>
					<li>1” compact foam base support.</li>
				</ul>
			</div>
			<div class="product-image col-lg-6 my-auto d-none d-lg-block">
				<img src="<?php echo get_home_url(); ?>/wp-content/uploads/2019/05/ExplorerSeries_ProductsImage-03-as-one.jpg" alt="ExplorerSeries_ProductsImage-03-as-one" class="lazy-load w-100 vc_single_image-img attachment-full is-loaded img-fluid">
			</div>
		</div>
	</div>
</div>
<?php get_footer() ?>