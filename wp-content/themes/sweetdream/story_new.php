<?php
/*
	Template Name: the-story-template
*/
get_header();
global $wpdb;
$get_all_stories = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."stories ORDER BY year");
?>
<div id="story-page" class="story-section container p-lg-0">
	<div class="story-inner-section">
		<h1 class="text-uppercase w-100">the story</h1>
  		<div class="storyline-section">
  			<div class="wrap-all-story">
  				<div id="stories" class="owl-carousel owl-theme">
					<?php foreach ($get_all_stories as $key => $get_all_story ) { ?>
						<div id="story-<?php echo $key ?>" class="item story-detail text-center">
							<h2><?php echo $get_all_story->year; ?></h2>
							<div class="story-dot"></div>
							<div class="story-line"></div>
							<div class="story-description">
								<?php echo str_replace('\"','',$get_all_story->description); ?>
							</div>
						</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
</div>
<?php get_footer() ?>